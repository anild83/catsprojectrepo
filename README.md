# CatsProjectRepo

**About the application**

* Service is developed using asp.net core web api
* Linq is used to sort and filter the list got from specified url
* HttpClient is used for making http request
* Unit tests are written using xUnit
* Moq is used as a mocking framework
* Front end applicatin is developed using Angular 5.2
* Please refer screen shots for output of the application(DisplayedOutput_WithCallToService.JPG) and unit test results(UnitTestResults.JPG) under Docs folder

**How to run the application**

* Open CatsProject solution in VS2017, rebuild and run
* Go to repo cats-app(or go to folder where cats-app is copied)
* Open terminal window and run “npm install”
* Run “npm start”
* If the port number has changed in iisexpress when you run the CatsProject, then change the url in cats.service.ts file in cats-app.



