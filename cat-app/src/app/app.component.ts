import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { CatsService } from './cats.service';
import { IOwner } from './owner';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  public owners: IOwner[];

  constructor(private catsService: CatsService) { }

  ngOnInit(): void {
    this.catsService.getOwnersWithCats().subscribe(owners => {
      this.owners = owners;
    });
  }
  
  title = 'Cats app';
}
