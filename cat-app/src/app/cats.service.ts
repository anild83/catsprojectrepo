﻿import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

import { Observable } from "rxjs/Observable";

import { IOwner } from "./owner";

@Injectable()
export class CatsService {
  constructor(
    private httpClient: HttpClient,
  ) { }

  public getOwnersWithCats(): Observable<IOwner[]>{
    return this.httpClient.get<IOwner[]>('http://localhost:59263/api/Cats/GetOwnersWithCats');
  }
}