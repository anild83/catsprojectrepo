﻿using CatsApi.Helpers;
using CatsApi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Xunit;

namespace CatsApi.Tests
{
    public class CatsHelperTests
    {

        [Fact]
        public void GetSortedCatList_WhenInputIsNull_ThrowsException()
        {
            //Act, Assert
            Assert.Throws<ArgumentNullException>(() => CatsHelper.GetSortedCatList(null));
        }

        [Fact]
        public void GetSortedCatList_WhenInputIsEmpty_ReturnsEmptyList()
        {
            //Act
            var result = CatsHelper.GetSortedCatList(new List<PetOwner>());

            //Assert
            Assert.Empty(result);

        }

        [Fact]
        public void GetSortedCatList_WhenUnsortedListIsPassed_ReturnsSortedList()
        {
            //Arrange
            var jsonInput = @"[{'name':'Bob','gender':'Male','age':23,'pets':[{'name':'Garfield','type':'Cat'},{'name':'Fido','type':'Dog'}]},{'name':'Jennifer','gender':'Female','age':18,'pets':[{'name':'Garfield','type':'Cat'}]},{'name':'Steve','gender':'Male','age':45,'pets':null},{'name':'Fred','gender':'Male','age':40,'pets':[{'name':'Tom','type':'Cat'},{'name':'Max','type':'Cat'},{'name':'Sam','type':'Dog'},{'name':'Jim','type':'Cat'}]},{'name':'Samantha','gender':'Female','age':40,'pets':[{'name':'Tabby','type':'Cat'}]},{'name':'Alice','gender':'Female','age':64,'pets':[{'name':'Simba','type':'Cat'},{'name':'Nemo','type':'Fish'}]}]";
            var unsortedPetOwners = JsonConvert.DeserializeObject<List<PetOwner>>(jsonInput);

            //Act
            var result = CatsHelper.GetSortedCatList(unsortedPetOwners);

            //Assert
            Assert.NotNull(result);
            Assert.Equal(2, result.Count);

            var catsOwnedByMale = result.Find(p => p.Gender.Equals("Male"));
            var catsOwnedByFemale = result.Find(p => p.Gender.Equals("Female"));

            Assert.Equal(4, catsOwnedByMale.Pets.Count);
            Assert.Equal(3, catsOwnedByFemale.Pets.Count);
            Assert.Equal(0, catsOwnedByMale.Pets.FindIndex(
                                            pet => pet.Name.Equals("Garfield")));
            Assert.Equal(1, catsOwnedByMale.Pets.FindIndex(
                                            pet => pet.Name.Equals("Jim")));
            Assert.Equal(2, catsOwnedByMale.Pets.FindIndex(
                                            pet => pet.Name.Equals("Max")));
            Assert.Equal(3, catsOwnedByMale.Pets.FindIndex(
                                            pet => pet.Name.Equals("Tom")));

            Assert.Equal(0, catsOwnedByFemale.Pets.FindIndex(
                                            pet => pet.Name.Equals("Garfield")));
            Assert.Equal(1, catsOwnedByFemale.Pets.FindIndex(
                                            pet => pet.Name.Equals("Simba")));
            Assert.Equal(2, catsOwnedByFemale.Pets.FindIndex(
                                            pet => pet.Name.Equals("Tabby")));

        }
        
    }
}
