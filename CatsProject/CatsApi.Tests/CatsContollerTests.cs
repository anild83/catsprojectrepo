﻿using CatsApi.Controllers;
using CatsApi.Helpers;
using CatsApi.Models;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace CatsApi.Tests
{
    public class CatsContollerTests
    {
        [Fact]
        public async void GetOwnersWithCats_ReturnsCatsData()
        {
            //Arrange
            var httpHelperMock = new Mock<IHttpClientHelper>();
            httpHelperMock.Setup(h => h.GetAsync<List<PetOwner>>(It.IsAny<string>()))
                            .Returns(Task.FromResult(new List<PetOwner>()));

            //Act
            var result = await new CatsController(httpHelperMock.Object).GetOwnersWithCats() as JsonResult;

            //Assert
            Assert.NotNull(result);
            Assert.IsType<List<PetOwner>>(result.Value);
            httpHelperMock.Verify(h => h.GetAsync<List<PetOwner>>(Constants.CatsGetUrl), Times.Once);
        }
    }
}
