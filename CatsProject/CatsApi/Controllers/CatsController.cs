﻿using CatsApi.Helpers;
using CatsApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CatsApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Cats")]
    public class CatsController : Controller
    {
        IHttpClientHelper _httpClientHelper;

        public CatsController(IHttpClientHelper httpClientHelper)
        {
            _httpClientHelper = httpClientHelper;
        }

        [AllowAnonymous]
        [Route("GetOwnersWithCats")]
        public async Task<IActionResult> GetOwnersWithCats()
        {
            var result = await _httpClientHelper.GetAsync<List<PetOwner>>(Constants.CatsGetUrl);

            return new JsonResult(CatsHelper.GetSortedCatList(result));
        }
    }
}