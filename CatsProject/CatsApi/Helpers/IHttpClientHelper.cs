﻿using System.Threading.Tasks;

namespace CatsApi.Helpers
{
    public interface IHttpClientHelper
    {
        Task<T> GetAsync<T>(string url);
    }
}
