﻿using CatsApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CatsApi.Helpers
{
    public class CatsHelper
    {
        public static List<PetOwner> GetSortedCatList(List<PetOwner> unSortedPets)
        {
            if (unSortedPets == null) { throw new ArgumentNullException(nameof(unSortedPets)); }

            return (from owner in unSortedPets
                    where owner.Pets != null && owner.Pets.Count > 0 && owner.Pets.Exists(t => t.Type.Equals("Cat"))
                    group owner by owner.Gender into groupedOwners
                    orderby groupedOwners.Key descending
                    let sortedPets = groupedOwners.SelectMany(s => s.Pets).Where(t => t.Type == "Cat").OrderBy(t => t.Name).ToList()
                    select new PetOwner
                    {
                        Gender = groupedOwners.Key,
                        Pets = sortedPets
                    }).ToList();
        }
    }
}
