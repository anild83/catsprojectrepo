﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;

namespace CatsApi.Helpers
{
    public class HttpClientHelper : IHttpClientHelper
    {
        private HttpClient _httpClientBkField;

        private HttpClient _httpClient => _httpClientBkField ?? (_httpClientBkField = new HttpClient());

        public async Task<T> GetAsync<T>(string url)
        {
            var response = await _httpClient.GetAsync(url);

            var content = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<T>(content);
        }
    }

}
